module.exports = {
    platform: "gitlab",
    endpoint: "https://gitlab.com/api/v4/",
    token: `glpat-GZwt7bBxJC7zsW3fmY28`,
    requireConfig: false,
    printConfig: true,
    pinDigests: true,
    ignoreTests: true,
    includeForks: true,
    dryRun: false,
    labels: [
      "renovate"
    ],
    packageRules: [
      {
        updateTypes: [
          "patch",
          "minor"
        ],
        groupName: "non-major.versions"
      },
      {
        updateTypes: [
          "pin",
          "digest"
        ],
        groupName: "pin and digest"
      }
    ]
};
  